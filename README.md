# Hashvatar

Implementation of [Hashvatar by François Best](https://francoisbest.com/posts/2021/hashvatars).

## Installation

The package can be installed by adding `hashvatar` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:hashvatar, "~> 1.0.0"}
  ]
end
```

## Usage

Example:

```elixir
<Hashvatar.hashvatar
  identifier={@user.uuid}
  variant={:stagger}
  line_color="#fffaf0"
/>
```

Options:

- `identifier` (required): the identifier used to generate the hashvatar
- `variant` (default: `:normal`): the variant, can be one of: `:normal`, `:stagger`, `:gem`, `:spider`, `:flower`
- `line_color` (default: `"white"`): the line color (as a CSS-like value)
- `radius_factor` (default: `0.42`): a coefficient for the circles radii

## Examples

Here are some examples with the identifier `Hello, world!` and the different variants:

### Normal

![normal](examples/normal.svg){width=100px height=100px}

### Stagger

![stagger](examples/stagger.svg){width=100px height=100px}

### Spider

![spider](examples/spider.svg){width=100px height=100px}

### Flower

![flower](examples/flower.svg){width=100px height=100px}

### Gem

![gem](examples/gem.svg){width=100px height=100px}

## Acknowledgement

This library was done as part of the work on [Kazarma](https://gitlab.com/kazarma/kazarma), which was funded [by NLnet](https://nlnet.nl/project/Kazarma-Release/#ack) through the [NGI0 Entrust](https://nlnet.nl/entrust/) fund.
