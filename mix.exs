defmodule Hashvatar.MixProject do
  use Mix.Project

  def project do
    [
      app: :hashvatar,
      version: "1.0.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package(),
      source_url: "https://gitlab.com/technostructures/lib/hashvatar.ex",
      homepage_url: "https://gitlab.com/technostructures/lib/hashvatar.ex"
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      {:phoenix_live_view, "~> 0.19.5"},
      {:phoenix_html, "~> 3.3"}
    ]
  end

  defp description do
    "Implementation of Hashvatar by François Best."
  end

  defp package do
    [
      files: ~w(lib .formatter.exs mix.exs README.md LICENSE.md),
      licenses: ["LGPL-3.0"],
      links: %{"GitLab" => "https://gitlab.com/technostructures/lib/hashvatar.ex"}
    ]
  end
end
